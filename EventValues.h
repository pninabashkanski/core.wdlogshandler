#pragma once
#include "stdafx.h"
#include "atlstr.h"
#include "XMLParser.h"
#include <string>

class EventValues
{
public:
	static std::string getEventID(std::string xml);
	static std::string getUserID(std::string xml);
	static std::string getDetectionTime(std::string xml);
	static std::string getMachine();
	static std::string getSignatureVersion(std::string xml);
	static std::string getEngineVersion(std::string xml);
	static std::string getProcessName(std::string xml);
	static std::string getThreatName(std::string xml);
	static std::string getSeverity(std::string xml);
	static std::string getCategoryName(std::string xml);
	static std::string getDetectionOriginId(std::string xml);
	static std::string getDetectionTypeID(std::string xml);
	static std::string getSourceID(std::string xml);
	static std::string getEventPath(std::string xml);
	static std::string getDetectionID(std::string xml);
	static std::string getOsVersion();
};