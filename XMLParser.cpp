#include "stdafx.h"
#include "atlstr.h"
#include "FileContent.h"
#include "XMLParser.h"

std::string getValue(std::string xml, int index) {
	char result[500];
	int count = 0;
	for (std::string::size_type i = index + 1; xml[i] != '<'; i++) {
		result[count] = xml[i];
		count++;
	}
	result[count] = '\0';
	return result;
}

std::string XMLParser::getXMLValue(std::string xml, std::string name) {
	int namePosition = xml.find(name);
	if (namePosition != std::string::npos) {
		for (std::string::size_type i = namePosition; i < xml.size(); ++i) {
			if (xml[i] == '>') {
				return getValue(xml, i);
				break;
			}
		}
	}
	return "";
}