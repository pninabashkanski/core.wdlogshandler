#include "stdafx.h"
#include "FileContent.h"
#include <strsafe.h>
#include "SubscriptionToEvents.h"
#include "EventValues.h"
#include "atlstr.h"
#include <ctime>


using namespace std;

PEVT_VARIANT pRenderedValues = NULL;
int counter = 0;

DWORD HandleEvent(EVT_HANDLE hEvent); // Shown in the Rendering Events topic
DWORD WINAPI SubscriptionCallback(EVT_SUBSCRIBE_NOTIFY_ACTION action, PVOID pContext, EVT_HANDLE hEvent);


void EventsSubscription::subscribe()
{
	while (true) {
		DWORD status = ERROR_SUCCESS;
		EVT_HANDLE hResults = NULL;
		LPWSTR pwsPath = L"Microsoft-Windows-Windows Defender/Operational";
		LPWSTR pwsQuery = L"Event/System[EventID=1007 or EventID=1117 or EventID=1008 or EventID=1118 or EventID=1119 or EventID=1006 or EventID=1015 or EventID=1116]";

		hResults = EvtSubscribe(NULL, NULL, pwsPath, pwsQuery, NULL, NULL, (EVT_SUBSCRIBE_CALLBACK)SubscriptionCallback, EvtSubscribeToFutureEvents);
		if (NULL == hResults)
		{
			status = GetLastError();

			if (ERROR_EVT_CHANNEL_NOT_FOUND == status)
				wprintf(L"The channel was not found.\n");
			else if (ERROR_EVT_INVALID_QUERY == status)
				wprintf(L"The query is not valid.\n");
			else
				wprintf(L"EvtQuery failed with %lu.\n", status);
			goto cleanup;
		}
		Sleep(20000);

	cleanup:

		if (hResults)
			EvtClose(hResults);

		if (pRenderedValues)
			free(pRenderedValues);

	}
}

// The callback that receives the events that match the query criteria. 
DWORD WINAPI SubscriptionCallback(EVT_SUBSCRIBE_NOTIFY_ACTION action, PVOID pContext, EVT_HANDLE hEvent)
{
	HandleEvent(hEvent);
	return 0;
}

string getCurrentTime() {
	time_t rawtime;
	struct tm timeinfo;
	char buffer[80];
	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);
	strftime(buffer, sizeof(buffer), "%Y-%m-%d-%H-%M-%S", &timeinfo);
	return buffer;
}

VOID WriteFileSanitized(HANDLE hFile, PCWCHAR writeBuffer)
{
	auto buffCopy = _wcsdup(writeBuffer);
	WriteFileSanitized(hFile, buffCopy);
	if (buffCopy)
		free(buffCopy);
}

void handleWriteToFile(string xml) {
	ofstream myfile;
	std::string eventID = EventValues::getEventID(xml);
	std::string path = "C:\\Program Files\\Morphisec\\logs\\WD\\";
	std::string file = path + getCurrentTime() + '_' + eventID + '_' + EventValues::getMachine() + '_' + std::to_string(counter) + ".dmp";
	counter < 20 ? counter++ : counter = 0;
	myfile.open(file, ios::in | ios::out | ios::app);
	BOOL isAction = (eventID == "1007" || eventID == "1117" || eventID == "1008" || eventID == "1118" || eventID == "1119");
	std::string body = isAction ? FileContent::ActionTypeBody(xml) : FileContent::DetectionTypeBody(xml);
	myfile << body;
	myfile.close();
}

DWORD HandleEvent(EVT_HANDLE hEvent)
{
	DWORD status = ERROR_SUCCESS;
	DWORD dwBufferSize = 0;
	DWORD dwBufferUsed = 0;
	DWORD dwPropertyCount = 0;
	LPWSTR pRenderedContent = NULL;
	std::string xml = "";

	// The EvtRenderEventXml flag tells EvtRender to render the event as an XML string.
	if (!EvtRender(NULL, hEvent, EvtRenderEventXml, dwBufferSize, pRenderedContent, &dwBufferUsed, &dwPropertyCount))
	{
		if (ERROR_INSUFFICIENT_BUFFER == (status = GetLastError()))
		{
			dwBufferSize = dwBufferUsed;
			pRenderedContent = (LPWSTR)malloc(dwBufferSize);
			if (pRenderedContent)
			{
				EvtRender(NULL, hEvent, EvtRenderEventXml, dwBufferSize, pRenderedContent, &dwBufferUsed, &dwPropertyCount);
			}
			else
			{
				wprintf(L"malloc failed\n");
				status = ERROR_OUTOFMEMORY;
				goto cleanup;
			}
		}

		if (ERROR_SUCCESS != (status = GetLastError()))
		{
			wprintf(L"EvtRender failed with %d\n", GetLastError());
			goto cleanup;
		}
	}

	xml = CW2A(pRenderedContent);
	handleWriteToFile(xml);
	wprintf(L"\n\n%s", pRenderedContent);

cleanup:

	if (pRenderedContent)
		free(pRenderedContent);

	return status;
};