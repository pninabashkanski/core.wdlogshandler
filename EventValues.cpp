#include "stdafx.h"
#include "atlstr.h"
#include "XMLParser.h"
#include "EventValues.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctime>
#include <string>
#include <vector>
#include "OsData.h"

using namespace std;

#pragma warning(disable : 4996)

#define INFO_BUFFER_SIZE 32767

vector<string> split(string str, string token) {
	vector<string> result;
	while (str.size()) {
		int index = str.find(token);
		if (index != string::npos) {
			result.push_back(str.substr(0, index));
			str = str.substr(index + token.size());
			if (str.size() == 0)result.push_back(str);
		}
		else {
			result.push_back(str);
			str = "";
		}
	}
	return result;
}

std::string getDateTimeInFormat(std::string dateTime) {
	int index = dateTime.find("T");
	std::string date = dateTime.substr(0, index);
	std::string time = dateTime.substr(index + 1, 8);
	return date + " " + time;
}

std::string getArrayOfPaths(std::string eventPath) {
	vector<string> splittedString = split(eventPath, ";");
	string result = "[";
	for (std::vector<int>::size_type i = 0; i != splittedString.size(); i++) {
		result += '"' + splittedString[i] + '"' + ",";
	}
	result = result.substr(0, result.size() - 1);
	result += "]";
	return result;
}

std::string EventValues::getEventID(string xml) {
	return XMLParser::getXMLValue(xml, "EventID");
}

std::string EventValues::getUserID(string xml) {
	return XMLParser::getXMLValue(xml, "Detection User");
}

std::string EventValues::getDetectionTime(string xml) {
	std::string dateTime =  XMLParser::getXMLValue(xml, "Detection Time");
	return getDateTimeInFormat(dateTime);
}

std::string EventValues::getMachine() {
	TCHAR  infoBuf[INFO_BUFFER_SIZE];
	DWORD  bufCharCount = INFO_BUFFER_SIZE;
	GetComputerName(infoBuf, &bufCharCount);
	wstring test(&infoBuf[0]);
	string result(test.begin(), test.end());
	return result;
}

std::string EventValues::getSignatureVersion(string xml) {
	return XMLParser::getXMLValue(xml, "Signature Version");
}

std::string EventValues::getEngineVersion(string xml) {
	return XMLParser::getXMLValue(xml, "Engine Version");
}

std::string EventValues::getProcessName(string xml) {
	return XMLParser::getXMLValue(xml, "Process Name");
}

std::string EventValues::getThreatName(string xml) {
	return XMLParser::getXMLValue(xml, "Threat Name");
}

std::string EventValues::getSeverity(string xml) {
	return XMLParser::getXMLValue(xml, "Severity Name");
}

std::string EventValues::getCategoryName(string xml) {
	return XMLParser::getXMLValue(xml, "Category Name");
}

std::string EventValues::getDetectionOriginId(std::string xml) {
	return XMLParser::getXMLValue(xml, "Origin ID");
}

std::string EventValues::getDetectionTypeID(std::string xml) {
	return XMLParser::getXMLValue(xml, "Type ID");
}

std::string EventValues::getSourceID(std::string xml) {
	return XMLParser::getXMLValue(xml, "Source ID");
}

std::string EventValues::getEventPath(std::string xml) {
	return getArrayOfPaths(XMLParser::getXMLValue(xml, "Path"));
}

std::string EventValues::getDetectionID(std::string xml) {
	std::string detection_id = XMLParser::getXMLValue(xml, "Detection ID");
	return detection_id.substr(1, detection_id.size() - 2);
}

std::string EventValues::getOsVersion()
{
	return 	OsData::getOsName();
}
