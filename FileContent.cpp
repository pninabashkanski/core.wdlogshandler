#include "stdafx.h"
#include "atlstr.h"
#include "FileContent.h"
#include "EventValues.h"

string convertFromVectorToJson(std::vector<pair<string, string>> v) {
	string res = "{";
	for (std::vector<int>::size_type i = 0; i != v.size(); i++) {
		if (v[i].first != "detection_path_ss") {
			res += '"' + v[i].first + '"' + ":" + '"' + v[i].second + '"' + ",";
		}
		else {
			res += '"' + v[i].first + '"' + ":"  + v[i].second + ",";
		}
	}
	res = res.substr(0, res.size() - 1);
	res += "}";
	return res;
}

std::vector<pair<string, string>> SetJsonCommonFields(string xml) {
	std::vector<pair<string, string>> e;
	e.push_back(std::make_pair("attack_time_dt", EventValues::getDetectionTime(xml)));
	e.push_back(std::make_pair("detection_id_s", EventValues::getDetectionID(xml)));
	return e;
}

string FileContent::ActionTypeBody(string xml) {
	std::vector<pair<string, string>> e = SetJsonCommonFields(xml);
	string EventID = EventValues::getEventID(xml);
	(EventID == "1007" || EventID == "1117") ? e.push_back(std::make_pair("wd_action_s", "SUCCESS")) : e.push_back(std::make_pair("wd_action_s", "FAIL"));
	e.push_back(std::make_pair("wd_operation_type_s", "ACTION"));
	return convertFromVectorToJson(e);
}

string FileContent::DetectionTypeBody(string xml) {
	std::vector<pair<string, string>> e = SetJsonCommonFields(xml);
	e.push_back(std::make_pair("wd_operation_type_s", "DETECTION"));
	e.push_back(std::make_pair("user_s", EventValues::getUserID(xml)));
	e.push_back(std::make_pair("machine_s", EventValues::getMachine()));
	e.push_back(std::make_pair("application_s", EventValues::getProcessName(xml)));
	//check
	e.push_back(std::make_pair("os_version_s", EventValues::getOsVersion()));

	e.push_back(std::make_pair("attack_name_s", EventValues::getThreatName(xml)));
	e.push_back(std::make_pair("attack_severity_i", EventValues::getSeverity(xml)));
	e.push_back(std::make_pair("attack_classification_sub_type_s", EventValues::getCategoryName(xml)));
	e.push_back(std::make_pair("detection_signature_version_s", EventValues::getSignatureVersion(xml)));
	e.push_back(std::make_pair("detection_engine_version_s", EventValues::getEngineVersion(xml)));
	e.push_back(std::make_pair("detection_path_ss", EventValues::getEventPath(xml)));
	e.push_back(std::make_pair("detection_origin_s", EventValues::getDetectionOriginId(xml)));
	e.push_back(std::make_pair("detection_type_s", EventValues::getDetectionTypeID(xml)));
	e.push_back(std::make_pair("detection_source_s", EventValues::getSourceID(xml)));
	return convertFromVectorToJson(e);
}