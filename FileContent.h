#pragma once
#include <windows.h>
#include <sddl.h>
#include <stdio.h>
#include <winevt.h>
#include <iostream>
#include <vector>
#include <utility>      // std::pair, std::make_pair
#include <string>

using namespace std;

class FileContent
{
public:
	static std::string ActionTypeBody(std::string xml);
	static std::string DetectionTypeBody(std::string xml);
};
